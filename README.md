# PlayMusic
Simple Rainmeter skin for Google Play Music

***PlayMusic***  
  Author=Brett Stevenson  
  Contact=BStevensonDev@gmail.com  
  Version=1.0

*********************************************************************************************************

INFO:  
  This Rainmeter skin is one that I, as a longtime GPM user,  have been looking for myself for a very long time.  
  It displays the Title, Artist, and Album of the current song being played, accompanied by the Album Artwork.  
  The skin also displays the current status of both the "shuffle" and "repeat" features.  
  Currently, the skin's features are fairly limited. Although keep an eye out for updates, as I will surely be   
  adding more features soon.


INSTRUCTIONS:  
  This skin requires you have GPMDP installed to function properly.   
  If not, you can download it here: http://www.googleplaymusicdesktopplayer.com/  
  You must also change the "USERNAME"  variable within the chosen ".ini" file, to fit the username for your computer.   
  (ie "C:\Users\#USERNAME#\") 
  
PREVIEW:  
  ![alt tag](https://github.com/JonSn0w/PlayMusic/blob/master/Preview/Square.png)  ![alt tag](https://github.com/JonSn0w/PlayMusic/blob/master/Preview/LandscapePreview.png)  

CREDITS:  
Thank you to all of the resources which inspired and/or aided the creation of this skin.  
  Primary resources linked below:  
     http://www.googleplaymusicdesktopplayer.com/   
     https://github.com/maarten1055/TestRainmeterGPMDP  
     https://github.com/MarshallOfSound/Google-Play-Music-Desktop-Player-UNOFFICIAL-  
